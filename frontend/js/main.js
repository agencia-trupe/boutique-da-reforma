(function($){

    $(document).ready(function() {

        $('#slider').cycle({
            fx: 'fade',
            speed: 'slow',
            slides: '> div',
            autoHeight: 'calc',
            pager: '#slider-pager',
            pagerTemplate: '<a href="#">{{slideNum}}</a>'
        });

        $('#depoimentos-slider').cycle({
            fx: 'fade',
            speed: 'fast',
            continueAuto: false,
            slides: '> div',
            pager: '#depoimentos-pager',
            autoHeight: 'container',
            pagerTemplate: '<a href="#">{{slideNum}}</a>'
        });

        $('.lightbox').fancybox({
            padding: 0,
            maxHeight: '80%',
            helpers: {
                overlay: {
                    locked: false
                }
            }
        });

        $('.servicos-home-thumb').hover(function() {
            if ($(window).width() >= 1060)
                $(this).find('p').slideToggle('fast');
        });

        $('.fotos-slider').slick({
            infinite: false,
            slidesToShow: 7,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1080,
                    settings: { slidesToShow: 5 }
                },
                {
                    breakpoint: 710,
                    settings: { slidesToShow: 3 }
                }
            ]
          });

        function selecionaFoto(event) {
            event.preventDefault();

            var clicked = $(this),
                wrapper = $('#foto-wrapper a');

            if (clicked.hasClass('active')) return false;

            wrapper.find('img').fadeOut();

            var foto = clicked.find('a').attr('href');

            $('.fotos-thumb').removeClass('active');
            clicked.addClass('active');

            var image = new Image();
            image.src = foto;
            image.wrapper = wrapper;

            image.onload = function () {
                $('#foto-wrapper a').empty().append(image).hide().fadeIn();
            };

            return false;
        }

        $('.fotos-thumb').on('click', selecionaFoto);
        $('.fotos-thumb.slick-active').first().trigger('click');

        function proximaFoto(event) {
            event.preventDefault();

            var next = $('.fotos-thumb.active').next();

            if (!next.length)
                return false;

            if(!next.hasClass('slick-active'))
                $('.slick-next').trigger('click');

            next.trigger('click');

        }

        $('#foto-wrapper a').on('click', proximaFoto);

        $('#contato-form').submit(function(event) {
            event.preventDefault();
            form = this;

            $.post('envioContato', {

                assunto  : $('#assunto').val(),
                nome     : $('#nome').val(),
                email    : $('#email').val(),
                mensagem : $('#mensagem').val()

            }, function(resposta){

                if (resposta.status == 'success') form.reset();
                $('.contato-resposta').hide().text(resposta.message).fadeIn('slow');

            }, 'json');
        });

        $('#mobile-trigger').on('click', function (event) {
            event.preventDefault();
            $('nav#mobile').slideToggle('fast');
        });

        var w = $(window);
        w.resize(function() {
            if (w.width() >= 710)
                $('nav#mobile').hide();
        });

    });

}(jQuery));