var gulp       = require('gulp'),
    stylus     = require('gulp-stylus'),
    jeet       = require('jeet'),
    rupture    = require('rupture'),
    koutoSwiss = require('kouto-swiss');
    jshint     = require('gulp-jshint'),
    uglify     = require('gulp-uglify'),
    concat     = require('gulp-concat'),
    rename     = require('gulp-rename'),
    notify     = require('gulp-notify');


var config = {

    dev: {

        stylus : './stylus',
        js     : './js/',
        vendor : './js/vendor'

    },

    build: {

        css : '../public/assets/css',
        js  : '../public/assets/js'

    },

    vendorPlugins: ['cycle.min.js', 'fancybox.alterado.js', 'slick.min.js']

};


gulp.task('cssBuild', function() {

    gulp.src([config.dev.stylus + '/main.styl'])
        .pipe(stylus({
            use: [
                koutoSwiss(), jeet(), rupture()
            ],
            compress: true
        }))
        .pipe(rename('main.min.css'))
        .pipe(gulp.dest(config.build.css))

});


gulp.task('jsHint', function() {

    gulp.src(config.dev.js + '/*.js')
        .pipe(jshint())
        .pipe(notify(function(file) {
            if (file.jshint.success) {
                return false;
            }

            var errors = file.jshint.results.map(function (data) {
                if (data.error) {
                    return data.error.reason + ' (line ' + data.error.line + ')';
                }
            }).join('\n');

            return 'JSHINT: ' + file.relative + ' (' + file.jshint.results.length + ' errors)\n' + errors;
        }));

});


gulp.task('jsBuild', function() {

    gulp.src(config.dev.js + '/main.js')
        .pipe(uglify())
        .on('error', function(error) {
            return notify().write('UGLIFY: ' + error.message + ' (' + error.fileName + ', line: ' + error.lineNumber + ')');
        })
        .pipe(rename('main.min.js'))
        .pipe(gulp.dest(config.build.js))

});


gulp.task('vendor', function() {

    for (var i=0 ; i < config.vendorPlugins.length; i++) {
        config.vendorPlugins[i] = config.dev.vendor + '/' + config.vendorPlugins[i];
    }

    gulp.src(config.vendorPlugins)
        .pipe(concat('vendor.min.js'))
        .pipe(uglify())
        .on('error', function(error) {
            return notify().write('UGLIFY: ' + error.message + ' (' + error.fileName + ', line: ' + error.lineNumber + ')');
        })
        .pipe(gulp.dest(config.build.js))

});


gulp.task('watch', function() {

    gulp.watch(config.dev.stylus + '/**/*.styl', ['cssBuild']);
    gulp.watch(config.dev.js + '/**/*.js', ['jsHint', 'jsBuild']);

});


gulp.task('default', ['cssBuild', 'jsHint', 'jsBuild', 'watch']);