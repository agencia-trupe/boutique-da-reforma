@section('content')

    <section id="depoimentos" class="conteudo">

        <div class="centro">
        @foreach($depoimentos as $depoimento)
            <div class="depoimento">
                @foreach($depoimento->imagens as $imagem)
                <a href="assets/img/depoimentos/{{$imagem->imagem}}" class="lightbox" rel="depoimentos-{{$depoimento->id}}">
                    <img src="assets/img/depoimentos/thumb/{{$imagem->imagem}}" alt="">
                </a>
                @endforeach
                <p class="texto">{{$depoimento->texto}}</p>
                <p class="nome">{{$depoimento->nome}}</p>
            </div>
        @endforeach
        </div>
        {{$depoimentos->links()}}

    </section>

@stop