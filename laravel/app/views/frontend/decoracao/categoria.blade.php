@section('content')

    <section id="servicos-categoria" class="conteudo">
    <div class="centro">
            <div class="categoria">
                <h1>{{$categoria->titulo}}</h1>
                <div class="descricao">{{$categoria->descricao}}</div>

                @foreach($subcategorias as $subcategoria)
                    <a class="subcategoria-thumb" href="decoracao/{{$categoria->slug}}/{{$subcategoria->slug}}">
                        <div>{{$subcategoria->titulo}}</div>
                        <img src="assets/img/decoracao/capa/{{$subcategoria->imagem_capa}}" alt="">
                    </a>
                @endforeach

                @if(sizeof($imagens) > 0)
                <div class="servicos-imagens">
                    @foreach($imagens as $imagem)
                        <a href="assets/img/decoracao/{{$imagem->imagem}}" class="lightbox" rel="categoria">
                            <img src="assets/img/decoracao/thumb/{{$imagem->imagem}}" alt="">
                        </a>
                    @endforeach
                </div>
                @endif
            </div>
            <aside>
                <ul>
                    @foreach($decoracoes as $decoracao)
                    <li><a href="decoracao/{{$decoracao->slug}}"@if($categoria->slug == $decoracao->slug) class="active"@endif>{{$decoracao->titulo}}</a></li>
                    @endforeach
                </ul>
                <div class="outros-servicos">
                    <h5>Conheça nossos outros serviços:</h5>
                    <a href="reforma-construcao">
                        <img src="assets/img/layout/aside-reforma.jpg" alt="">
                        <span>Reforma e Construção</span>
                    </a>
                    <a href="marcenaria">
                        <img src="assets/img/layout/aside-marcenaria.jpg" alt="">
                        <span>Marcenaria</span>
                    </a>
                </div>
            </aside>
        </div>
    </section>

@stop