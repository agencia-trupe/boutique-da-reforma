@section('content')

    <section id="servicos-home" class="conteudo">

    <div class="centro">
        @foreach($decoracoes as $decoracao)
        <a href="decoracao/{{$decoracao->slug}}" class="servicos-home-thumb">
            <div class="imagem">
                <img src="assets/img/decoracao/thumb_home/{{$decoracao->thumb}}" alt="">
            </div>
            <h2>{{$decoracao->titulo}}</h2>
            <p>{{$decoracao->olho}}</p>
        </a>
        @endforeach
    </div>

    </section>

@stop