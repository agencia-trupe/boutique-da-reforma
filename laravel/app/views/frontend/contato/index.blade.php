@section('content')

    <section id="contato" class="conteudo">

        <div class="centro">
            <div class="texto">
                <p class="frase">Aguardamos o seu contato, <br>solicite um orçamento ou tire suas dúvidas!</p>
                <p class="informacoes">
                    <span>{{$contato->telefone_contato}}</span>
                    <a href="mailto:{{$contato->email_contato}}">{{$contato->email_contato}}</a>
                </p>
            </div>

            <form action="" method="post" id="contato-form">
                <div class="left">
                    <input type="text" name="assunto" id="assunto" placeholder="Assunto">
                    <input type="text" name="nome" id="nome" placeholder="Nome" required>
                    <input type="email" name="email" id="email" placeholder="E-mail" required>
                </div>
                <div class="right">
                    <textarea name="mensagem" id="mensagem" placeholder="Mensagem" required></textarea>
                    <input type="submit" value="Enviar">
                    <div class="contato-resposta"></div>
                </div>
            </form>
        </div>

    </section>

@stop