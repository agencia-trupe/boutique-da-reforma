@section('content')

    <section id="servicos-categoria" class="conteudo">
    <div class="centro">
            <div class="categoria">
                <h2>Marcenaria</h2>
                <h1>{{$categoria->titulo}}</h1>
                <div class="descricao">{{$categoria->descricao}}</div>

                @if(sizeof($imagens) > 0)
                <div class="servicos-imagens">
                    @foreach($imagens as $imagem)
                        <a href="assets/img/marcenaria/{{$imagem->imagem}}" class="lightbox" rel="categoria">
                            <img src="assets/img/marcenaria/thumb/{{$imagem->imagem}}" alt="">
                        </a>
                    @endforeach
                </div>
                @endif

                <div class="voltar">
                    <a href="marcenaria">voltar</a>
                </div>
            </div>
            <aside>
                <img src="assets/img/marcenaria/capa_home/{{$marcenaria->imagem_capa}}" class="marcenaria-capa" alt="">
                <div class="outros-servicos">
                    <h5>Conheça nossos outros serviços:</h5>
                    <a href="reforma-construcao">
                        <img src="assets/img/layout/aside-reforma.jpg" alt="">
                        <span>Reforma e Construção</span>
                    </a>
                    <a href="decoracao">
                        <img src="assets/img/layout/aside-decoracao.jpg" alt="">
                        <span>Decoração</span>
                    </a>
                </div>
            </aside>
        </div>
    </section>

@stop