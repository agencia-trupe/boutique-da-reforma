@if(sizeof($depoimentos_footer))
<div class="footer-depoimentos">
    <div class="centro">
        <div id="depoimentos-slider">
            @foreach($depoimentos_footer as $depoimento)
                <div class="depoimento">
                    @foreach($depoimento->imagens as $imagem)
                    <a href="assets/img/depoimentos/{{$imagem->imagem}}" class="lightbox" rel="depoimentos-{{$depoimento->id}}">
                        <img src="assets/img/depoimentos/thumb/{{$imagem->imagem}}" alt="">
                    </a>
                    @endforeach
                    <p class="texto">{{$depoimento->texto}}</p>
                    <p class="nome">{{$depoimento->nome}}</p>
                </div>
            @endforeach
        </div>
        <div id="depoimentos-pager"></div>
        <a href="depoimentos" class="mais-depoimentos">Mais depoimentos</a>
    </div>
</div>
@endif