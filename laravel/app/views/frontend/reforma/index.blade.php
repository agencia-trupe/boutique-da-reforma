@section('content')

    <section id="servicos-home" class="conteudo">

    <div class="centro">
        @foreach($reformas as $reforma)
        <a href="reforma-construcao/{{$reforma->slug}}" class="servicos-home-thumb">
            <div class="imagem">
                <img src="assets/img/reforma/thumb_home/{{$reforma->thumb}}" alt="">
            </div>
            <h2>{{$reforma->titulo}}</h2>
            <p>{{$reforma->olho}}</p>
        </a>
        @endforeach
    </div>

    </section>

@stop