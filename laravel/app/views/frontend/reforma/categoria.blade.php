@section('content')

    <section id="servicos-categoria" class="conteudo">
    <div class="centro">
            <div class="categoria">
                <h1>{{$categoria->titulo}}</h1>
                <div class="descricao">{{$categoria->descricao}}</div>

                @foreach($subcategorias as $subcategoria)
                    <a class="subcategoria-thumb" href="reforma-construcao/{{$categoria->slug}}/{{$subcategoria->slug}}">
                        <div>{{$subcategoria->titulo}}</div>
                        <img src="assets/img/reforma/capa/{{$subcategoria->imagem_capa}}" alt="">
                    </a>
                @endforeach

                @if(sizeof($imagens) > 0)
                <div class="servicos-imagens">
                    @foreach($imagens as $imagem)
                        <a href="assets/img/reforma/{{$imagem->imagem}}" class="lightbox" rel="categoria">
                            <img src="assets/img/reforma/thumb/{{$imagem->imagem}}" alt="">
                        </a>
                    @endforeach
                </div>
                @endif
            </div>
            <aside>
                <ul>
                    @foreach($reformas as $reforma)
                    <li><a href="reforma-construcao/{{$reforma->slug}}"@if($categoria->slug == $reforma->slug) class="active"@endif>{{$reforma->titulo}}</a></li>
                    @endforeach
                </ul>
                <div class="outros-servicos">
                    <h5>Conheça nossos outros serviços:</h5>
                    <a href="decoracao">
                        <img src="assets/img/layout/aside-decoracao.jpg" alt="">
                        <span>Decoração</span>
                    </a>
                    <a href="marcenaria">
                        <img src="assets/img/layout/aside-marcenaria.jpg" alt="">
                        <span>Marcenaria</span>
                    </a>
                </div>
            </aside>
        </div>
    </section>

@stop