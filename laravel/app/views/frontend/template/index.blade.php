<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="author" content="Trupe Design">
    <meta name="copyright" content="2015 Trupe Design">

    <meta name="description" content="Boutique da Reforma">
    <meta name="keywords" content="" />
    <meta property="og:title" content="Boutique da Reforma">
    <meta property="og:description" content="Boutique da Reforma">
    <meta property="og:site_name" content="Boutique da Reforma">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="og:image" content="{{url()}}/assets/img/layout/logo.png">

    <title>Boutique da Reforma</title>

    <base href="{{ url() }}/">
    <script>var BASE = "{{ url() }}"</script>

    <link rel="stylesheet" href="assets/css/main.min.css">
</head>
<body>
    <nav id="mobile">
        <a href="home"@if(str_contains('home', Route::currentRouteName())) class="active"@endif>Home</a>
        <a href="reforma-construcao"@if(str_contains(Route::currentRouteName(), 'reforma')) class="active"@endif>Reforma e Construção</a>
        <a href="decoracao"@if(str_contains(Route::currentRouteName(), 'decoracao')) class="active"@endif>Decoração</a>
        <a href="marcenaria"@if(str_contains(Route::currentRouteName(), 'marcenaria')) class="active"@endif>Marcenaria</a>
        <a href="fotos"@if(str_contains(Route::currentRouteName(), 'fotos')) class="active"@endif>Fotos</a>
        <a href="contato"@if(str_contains(Route::currentRouteName(), 'contato')) class="active"@endif>Contato</a>
    </nav>
    <header>
        <div class="centro">
            <h1><a href="home"><img src="{{url('assets/img/layout/logo.png')}}" alt="Boutique da Reforma"></a></h1>
            <a href="#" id="mobile-trigger"></a>
            <nav id="desktop">
                <a href="home"@if(str_contains('home', Route::currentRouteName())) class="active"@endif>Home</a>
                <a href="reforma-construcao"@if(str_contains(Route::currentRouteName(), 'reforma')) class="active"@endif>Reforma e Construção</a>
                <a href="decoracao"@if(str_contains(Route::currentRouteName(), 'decoracao')) class="active"@endif>Decoração</a>
                <a href="marcenaria"@if(str_contains(Route::currentRouteName(), 'marcenaria')) class="active"@endif>Marcenaria</a>
                <a href="fotos"@if(str_contains(Route::currentRouteName(), 'fotos')) class="active"@endif>Fotos</a>
                <a href="contato"@if(str_contains(Route::currentRouteName(), 'contato')) class="active"@endif>Contato</a>
            </nav>
            <ul class="social">
                @if($contato->facebook)<li><a href="{{$contato->facebook}}" target="_blank" class="facebook">facebook</a></li>@endif
                @if($contato->twitter)<li><a href="{{$contato->twitter}}" target="_blank" class="twitter">twitter</a></li>@endif
            </ul>
        </div>
    </header>
    @yield('content')

    @if(!in_array(Route::currentRouteName(), array('contato', 'home')))
        @include('frontend.partials.footer_orcamento')
    @endif

    @if(!in_array(Route::currentRouteName(), array('contato', 'depoimentos')))
        @include('frontend.partials.footer_depoimentos')
    @endif

    <footer>
        <div class="centro">
            <nav class="footer-links">
                <a href="home">Home</a>
                <a href="marcenaria">Marcenaria</a>
                <a href="fotos">Fotos</a>
                <a href="contato">Contato</a>
            </nav>
            <nav class="footer-links">
                <a href="reforma-construcao">Reforma e Construção</a>
                <ul>
                @foreach($reforma_categorias as $reforma)
                    <li><a href="reforma-construcao/{{$reforma->slug}}">{{$reforma->titulo}}</a></li>
                @endforeach
                </ul>
            </nav>
            <nav class="footer-links">
                <a href="decoracao">Decoração</a>
                <ul>
                @foreach($decoracao_categorias as $decoracao)
                    <li><a href="decoracao/{{$decoracao->slug}}">{{$decoracao->titulo}}</a></li>
                @endforeach
                </ul>
            </nav>
            <div class="informacoes">
                <img src="assets/img/layout/footer-detalhe.png" alt="">
                <p class="telefone">{{$contato->telefone_contato}}</p>
                <a class="email" href="mailto:{{$contato->email_contato}}">{{$contato->email_contato}}</a>
                <div class="copyright">
                    <p>©2015 Todos os direitos reservados</p>
                    <p>
                        <a href="http://www.trupe.net" target="_blank">Criação de sites:</a> <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery-2.1.3.min.js"><\/script>')</script>
    <script src="assets/js/vendor.min.js"></script>
    <script src="assets/js/main.min.js"></script>
</body>
</html>