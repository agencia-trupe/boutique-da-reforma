@section('content')

    <section id="fotos" class="conteudo">
        <div class="centro">
            <div class="fotos-slider">
            @foreach($fotos as $foto)
                <div class="fotos-thumb">
                    <a href="assets/img/fotos/{{$foto->imagem}}">
                        <img src="assets/img/fotos/thumb/{{$foto->imagem}}" alt="">
                    </a>
                </div>
            @endforeach
            </div>
        </div>
        <div class="foto-outer">
            <div id="foto-wrapper" class="centro">
                <a href="#"></a>
            </div>
        </div>
    </section>

@stop