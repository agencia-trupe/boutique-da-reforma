@section('content')

    <section id="home">

        <div class="slider-wrapper">
            <div id="slider" class="centro">
            @foreach($slides as $slide)
                <div class="slide">
                    <div class="slider-texto">
                        <h2>{{$slide->titulo}}</h2>
                        <p>{{$slide->descricao}}</p>
                        <a href="{{$slide->link}}">Saiba mais</a>
                    </div>
                    <div class="slider-imagem">
                        <img src="assets/img/slides/{{$slide->imagem}}" alt="">
                    </div>
                </div>
            @endforeach
            </div>
        </div>
        <div id="slider-pager"></div>

        <h3 class="frase">Tudo para reforma e decoração de imóveis</h3>

        <div class="chamadas centro">
            @foreach($chamadas as $chamada)
                <a href="{{$chamada->link}}">
                    <img src="assets/img/chamadas/{{$chamada->imagem}}" alt="">
                    <h4>{{$chamada->titulo}}</h1>
                    <p>{{$chamada->descricao}}</p>
                </a>
            @endforeach
        </div>

    </section>

@stop