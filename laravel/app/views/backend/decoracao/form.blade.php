@section('content')

    <legend>
        <h2><small>Decoração /</small> Adicionar Categoria</h2>
    </legend>

    <form action="{{URL::route('painel.decoracao.store')}}" method="post" enctype="multipart/form-data">
        <div class="pad">

            @if(Session::has('sucesso'))
               <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
            @endif

            @if($errors->any())
                <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
            @endif

            <div class="form-group">
                <label for="inputTitulo">Título</label>
                <input type="text" class="form-control" id="inputTitulo" name="titulo"  @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
            </div>

            <div class="form-group">
                <label for="inputOlho">Olho</label>
                <textarea name="olho" id="inputOlho" class="form-control" rows="3" required>@if(Session::has('formulario')){{ Session::get('formulario.olho') }}@endif</textarea>
            </div>

            <div class="form-group">
                <label for="inputDesc">Descrição</label>
                <textarea name="descricao" id="inputDesc" class="form-control" rows="6" required>@if(Session::has('formulario')){{ Session::get('formulario.descricao') }}@endif</textarea>
            </div>

            <div class="form-group">
                <label for="inputImagem">Imagem de capa <small>(214x214 px)</small></label>
                <input type="file" class="form-control" id="inputImagem" name="thumb" required>
            </div>

            <button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

            <a href="{{URL::route('painel.decoracao.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

        </div>
    </form>

@stop