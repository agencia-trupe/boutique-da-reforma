@section('content')

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <a href="{{URL::route('painel.marcenaria.index')}}" title="Voltar para Marcenaria" class="btn btn-default">&larr; Voltar para Marcenaria</a>

    <h2>
        Imagens de: @if($categoria=='Marcenaria')Marcenaria
        @else{{$categoria->titulo}}@endif
        <a href="{{ URL::route('painel.marcenariaImagens.create', array('marcenaria_categorias_id' => $marcenaria_categorias_id)) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span>  Adicionar Foto</a>
    </h2>

    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Imagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($imagens as $imagem)

                <tr class="tr-row">
                    <td><img src="{{url('assets/img/marcenaria/thumb/'.$imagem->imagem)}}"></td>
                    <td class="crud-actions">
                       {{ Form::open(array('route' => array('painel.marcenariaImagens.destroy', $imagem->id), 'method' => 'delete')) }}
                        <input type="hidden" name="marcenaria_categorias_id" value="{{$marcenaria_categorias_id}}">
                            <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                       {{ Form::close() }}
                    </td>
                </tr>

            @endforeach
        </tbody>
    </table>

</div>

@stop