@section('content')

    <legend>
        <h2><small>Reforma e Construção /</small> Editar Categoria</h2>
    </legend>

    {{ Form::open( array('route' => array('painel.reforma.update', $categoria->id), 'files' => true, 'method' => 'put') ) }}
        <div class="pad">

            @if(Session::has('sucesso'))
               <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
            @endif

            @if($errors->any())
                <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
            @endif

            <div class="form-group">
                <label for="inputTitulo">Título</label>
                <input type="text" class="form-control" id="inputTitulo" name="titulo" value="{{$categoria->titulo}}" required>
            </div>

            <div class="form-group">
                <label for="inputOlho">Olho</label>
                <input type="text" class="form-control" id="inputOlho" name="olho" value="{{$categoria->olho}}" required>
            </div>

            <div class="form-group">
                <label for="inputDesc">Descrição</label>
                <textarea name="descricao" id="inputDesc" class="form-control" rows="6" required>{{$categoria->descricao}}</textarea>
            </div>

            <div class="form-group">
                <label>Imagem de capa atual</label><br>
                <img src="{{url('assets/img/reforma/thumb_home/'.$categoria->thumb)}}" alt=""><br>
                <label for="inputImagem">Trocar Imagem <small>(214x214 px)</small></label>
                <input type="file" class="form-control" id="inputImagem" name="thumb">
            </div>

            <button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

            <a href="{{URL::route('painel.reforma.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

        </div>
    </form>

@stop