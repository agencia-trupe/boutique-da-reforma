@section('content')

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <legend>
        <h2>
            Reforma e Construção
            <a href="{{ URL::route('painel.reforma.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Categoria</a>
        </h2>
    </legend>

    @if(sizeof($categorias))
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Capa</th>
                    <th>Título</th>
                    <th>Imagens</th>
                    <th>Subcategorias</th>
                    <th><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categorias as $categoria)
                <tr>
                    <td><img src="{{url('assets/img/reforma/thumb_home/'.$categoria->thumb)}}" alt="" style="max-width: 150px;"></td>
                    <td>{{$categoria->titulo}}</td>
                    <td>
                        <a href="{{URL::route('painel.reformaImagens.index', array('reforma_id' => $categoria->id))}}" class="btn btn-sm btn-default">gerenciar</a>
                    </td>
                    <td>
                        <a href="{{URL::route('painel.reformaCategorias.index', array('reforma_id' => $categoria->id))}}" class="btn btn-sm btn-default">gerenciar</a>
                    </td>
                    <td class="crud-actions">
                        <a href="{{ URL::route('painel.reforma.edit', $categoria->id ) }}" class="btn btn-primary btn-sm pull-left">editar</a>
                       {{ Form::open(array('route' => array('painel.reforma.destroy', $categoria->id), 'method' => 'delete')) }}
                            <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="alert alert-warning" role="alert">Nenhuma categoria cadastrada, utilize o menu superior para inserir categorias.</div>
    @endif

@stop