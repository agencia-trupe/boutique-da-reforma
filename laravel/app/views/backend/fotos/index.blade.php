@section('content')

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <legend>
        <h2>
            Fotos
            <a href="{{ URL::route('painel.fotos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Foto</a>
        </h2>
    </legend>

    <table class='table table-bordered table-hover'>
        <thead>
            <tr>
                <th>Foto</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>
        <tbody>
            @foreach($fotos as $foto)
            <tr class="tr-row">
                <td><img src="{{url('assets/img/fotos/thumb/'.$foto->imagem)}}" alt=""></td>
                <td class="crud-actions">
                    {{ Form::open(array('route' => array('painel.fotos.destroy', $foto->id), 'method' => 'delete')) }}
                         <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                    {{ Form::close() }}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{$fotos->links()}}

@stop