@section('content')

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <a href="{{URL::route('painel.decoracao.index')}}" title="Voltar para Decoração" class="btn btn-default">&larr; Voltar para Decoração</a>

    <legend>
        <h2>
            <small>Decoração / </small> {{$categoria->titulo}}
            <a href="{{ URL::route('painel.decoracaoCategorias.create', array('decoracao_id' => $decoracao_id)) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Subcategoria</a>
        </h2>
    </legend>

    @if(sizeof($subcategorias))
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Capa</th>
                    <th>Título</th>
                    <th>Imagens</th>
                    <th><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($subcategorias as $subcategoria)
                <tr>
                    <td><img src="{{url('assets/img/decoracao/capa/'.$subcategoria->imagem_capa)}}" alt=""></td>
                    <td>{{$subcategoria->titulo}}</td>
                    <td>
                        <a href="{{URL::route('painel.decoracaoImagens.index', array('decoracao_categoria_id' => $subcategoria->id))}}" class="btn btn-sm btn-default">gerenciar</a>
                    </td>
                    <td class="crud-actions">
                        <a href="{{ URL::route('painel.decoracaoCategorias.edit', array($subcategoria->id, 'decoracao_id' => $decoracao_id)) }}" class="btn btn-primary btn-sm pull-left">editar</a>
                       {{ Form::open(array('route' => array('painel.decoracaoCategorias.destroy', $subcategoria->id), 'method' => 'delete')) }}
                            <input type="hidden" name="decoracao_id" value="{{$decoracao_id}}">
                            <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="alert alert-warning" role="alert">Nenhuma subcategoria cadastrada, utilize o menu superior para inserir subcategorias.</div>
    @endif

@stop