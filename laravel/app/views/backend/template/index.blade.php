<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Boutique da Reforma - Painel Administrativo</title>
    <meta name="description" content="Boutique da Reforma">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/painel.min.css')}}">
</head>
<body @if(Route::currentRouteName() == 'painel.login')class="login"@else class="painel"@endif>
    @if(Route::currentRouteName() != 'painel.login')
    <nav class="navbar navbar-default">
        <a href="{{url()}}" class="navbar-brand">Boutique da Reforma</a>
        <ul class="nav navbar-nav">
            <li @if(str_is('painel.home',Route::currentRouteName())) class="active" @endif>
                <a href="{{URL::route('painel.home')}}">Início</a>
            </li>
            <li class="dropdown @if(preg_match('~painel.(slides|chamadas)~', Route::currentRouteName())) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Home <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{{URL::route('painel.slides.index')}}">Slides</a></li>
                    <li><a href="{{URL::route('painel.chamadas.index')}}">Chamadas</a></li>
                </ul>
            </li>
            <li class="dropdown @if(preg_match('~painel.(reforma|decoracao|marcenaria)~', Route::currentRouteName())) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Serviços <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{{URL::route('painel.reforma.index')}}">Reforma e Construção</a></li>
                    <li><a href="{{URL::route('painel.decoracao.index')}}">Decoração</a></li>
                    <li><a href="{{URL::route('painel.marcenaria.index')}}">Marcenaria</a></li>
                </ul>
            </li>
            <li @if(str_is('painel.fotos*',Route::currentRouteName())) class="active" @endif>
                <a href="{{URL::route('painel.fotos.index')}}">Fotos</a>
            </li>
            <li @if(str_is('painel.depoimentos*',Route::currentRouteName())) class="active" @endif>
                <a href="{{URL::route('painel.depoimentos.index')}}">Depoimentos</a>
            </li>
            <li class="dropdown @if(preg_match('~painel.(contato|contatosrecebidos)~', Route::currentRouteName())) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contato <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{{URL::route('painel.contato.index')}}">Informações de Contato</a></li>
                    <li><a href="{{URL::route('painel.contatosRecebidos.index')}}">Contatos Recebidos</a></li>
                </ul>
            </li>
            <li class="dropdown @if(str_is('painel.usuarios*', Route::currentRouteName())) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{{URL::route('painel.usuarios.index')}}">Usuários</a></li>
                    <li><a href="{{URL::route('painel.off')}}">Logout</a></li>
                </ul>
            </li>
        </ul>
    </nav>
    @endif

    <div @if(Route::currentRouteName() == 'painel.login')class="wrapper"@else class="main container"@endif>
    @yield('content')
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery-2.1.3.min.js"><\/script>')</script>
    <script src="{{url('assets/js/jquery-ui.min.js')}}"></script>
    <script src="{{url('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{url('assets/js/painel.js')}}"></script>
</body>
</html>