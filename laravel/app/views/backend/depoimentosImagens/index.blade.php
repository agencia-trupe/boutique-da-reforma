@section('content')

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <a href="{{URL::route('painel.depoimentos.index')}}" title="Voltar para Depoimentos" class="btn btn-default">&larr; Voltar para Depoimentos</a>

    <h2>
        Imagens do Depoimento de: {{$depoimento->nome}}
        @if(sizeof($imagens) < $limiteRegistros)
            <a href="{{ URL::route('painel.depoimentosImagens.create', array('depoimento_id' => $depoimento->id)) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span>  Adicionar Foto</a>
        @endif
    </h2>

    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Imagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($imagens as $imagem)

                <tr class="tr-row">
                    <td><img src="{{url('assets/img/depoimentos/thumb/'.$imagem->imagem)}}"></td>
                    <td class="crud-actions">
                       {{ Form::open(array('route' => array('painel.depoimentosImagens.destroy', $imagem->id), 'method' => 'delete')) }}
                        <input type="hidden" name="depoimento_id" value="{{$depoimento->id}}">
                            <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                       {{ Form::close() }}
                    </td>
                </tr>

            @endforeach
        </tbody>
    </table>

</div>

@stop