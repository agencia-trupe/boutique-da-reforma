@section('content')

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <a href="{{URL::route('painel.decoracao.index')}}" title="Voltar para Reforma e Construção" class="btn btn-default">&larr; Voltar para Decoração</a>

    <h2>
        Imagens de: {{$current['titulo']}}
        <a href="{{ URL::route('painel.decoracaoImagens.create', array($current['property'] => $current['id'])) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span>  Adicionar Foto</a>
    </h2>

    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Imagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($imagens as $imagem)

                <tr class="tr-row">
                    <td><img src="{{url('assets/img/decoracao/thumb/'.$imagem->imagem)}}"></td>
                    <td class="crud-actions">
                       {{ Form::open(array('route' => array('painel.decoracaoImagens.destroy', $imagem->id), 'method' => 'delete')) }}
                        <input type="hidden" name="route_property" value="{{$current['property']}}">
                        <input type="hidden" name="route_id" value="{{$current['id']}}">
                            <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                       {{ Form::close() }}
                    </td>
                </tr>

            @endforeach
        </tbody>
    </table>

</div>

@stop