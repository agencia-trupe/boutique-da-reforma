@section('content')

    <legend>
        <h2><small>{{$subcategoria->titulo}} /</small> Editar Subcategoria</h2>
    </legend>

    {{ Form::open( array('route' => array('painel.reformaCategorias.update', $subcategoria->id), 'files' => true, 'method' => 'put') ) }}
        <div class="pad">

            @if(Session::has('sucesso'))
               <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
            @endif

            @if($errors->any())
                <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
            @endif

            <div class="form-group">
                <label for="inputTitulo">Título</label>
                <input type="text" class="form-control" id="inputTitulo" name="titulo" value="{{$subcategoria->titulo}}" required>
            </div>

            <div class="form-group">
                <label for="inputDesc">Descrição</label>
                <textarea name="descricao" id="inputDesc" class="form-control" rows="6" required>{{$subcategoria->descricao}}</textarea>
            </div>

            <div class="form-group">
                <label>Imagem de capa atual</label><br>
                <img src="{{url('assets/img/reforma/capa/'.$subcategoria->imagem_capa)}}" alt=""><br>
                <label for="inputImagem">Trocar Imagem <small>(420x120 px)</small></label>
                <input type="file" class="form-control" id="inputImagem" name="imagem_capa">
            </div>

            <input type="hidden" name="reforma_id" value="{{$reforma_id}}">

            <button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

            <a href="{{URL::route('painel.reformaCategorias.index', array('reforma_id' => $reforma_id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

        </div>
    </form>

@stop