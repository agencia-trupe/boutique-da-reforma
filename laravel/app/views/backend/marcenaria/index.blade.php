@section('content')

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <legend>
        <h2>
            Marcenaria
            <div class="btn-group pull-right">
                <a href="{{ URL::route('painel.marcenaria.edit', $marcenaria->id) }}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-pencil"></span> Editar</a>
                <a href="{{ URL::route('painel.marcenariaImagens.index', array('marcenaria_categorias_id' => 0)) }}" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-picture"></span> Gerenciar Imagens</a>
                <a href="{{ URL::route('painel.marcenariaCategorias.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Categoria</a>
            </div>
        </h2>
    </legend>

    @if(sizeof($categorias))
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Capa</th>
                    <th>Título</th>
                    <th>Imagens</th>
                    <th><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categorias as $categoria)
                <tr>
                    <td><img src="{{url('assets/img/marcenaria/capa/'.$categoria->imagem_capa)}}" alt=""></td>
                    <td>{{$categoria->titulo}}</td>
                    <td>
                        <a href="{{URL::route('painel.marcenariaImagens.index', array('marcenaria_categorias_id' => $categoria->id))}}" class="btn btn-sm btn-default">gerenciar</a>
                    </td>
                    <td class="crud-actions">
                        <a href="{{ URL::route('painel.marcenariaCategorias.edit', $categoria->id ) }}" class="btn btn-primary btn-sm pull-left">editar</a>
                       {{ Form::open(array('route' => array('painel.marcenariaCategorias.destroy', $categoria->id), 'method' => 'delete')) }}
                            <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="alert alert-warning" role="alert">Nenhuma categoria cadastrada, utilize o menu superior para inserir categorias.</div>
    @endif

@stop