@section('content')

    <legend>
        <h2>Editar Marcenaria</h2>
    </legend>

    {{ Form::open( array('route' => array('painel.marcenaria.update', $marcenaria->id), 'files' => true, 'method' => 'put') ) }}
        <div class="pad">

            @if(Session::has('sucesso'))
                <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
            @endif

            @if($errors->any())
                <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
            @endif

            <div class="form-group">
                <label for="inputDesc">Descrição</label>
                <textarea name="descricao" id="inputDesc" class="form-control" rows="6" required>{{$marcenaria->descricao}}</textarea>
            </div>

            <div class="form-group">
                <label>Imagem de capa atual</label><br>
                <img src="{{url('assets/img/marcenaria/capa_home/'.$marcenaria->imagem_capa)}}" alt=""><br><br>
                <label for="inputImagem">Trocar Imagem <small>(280x250 px)</small></label>
                <input type="file" class="form-control" id="inputImagem" name="imagem">
            </div>

            <button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

            <a href="{{URL::route('painel.marcenaria.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

        </div>
    </form>

@stop