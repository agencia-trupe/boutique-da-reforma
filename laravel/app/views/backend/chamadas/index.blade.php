@section('content')

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <legend>
        <h2><small>Home /</small> Chamadas</h2>
    </legend>

    <table class='table table-bordered table-hover'>
        <thead>
            <tr>
                <th>Imagem</th>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>
        <tbody>
            @foreach($chamadas as $chamada)
            <tr class="tr-row">
                <td><img src="{{url('assets/img/chamadas/'.$chamada->imagem)}}" style="max-width: 150px" alt=""></td>
                <td>{{ $chamada->titulo }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.chamadas.edit', $chamada->id ) }}' class='btn btn-primary btn-sm'>editar</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@stop