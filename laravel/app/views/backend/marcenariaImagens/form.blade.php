@section('content')

    <legend>
        <h2>Adicionar Imagem a: @if($categoria=='Marcenaria')Marcenaria
        @else{{$categoria->titulo}}@endif</h2>
    </legend>

    <form action="{{URL::route('painel.marcenariaImagens.store')}}" method="post" enctype="multipart/form-data">
        <div class="pad">

            @if(Session::has('sucesso'))
               <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
            @endif

            @if($errors->any())
                <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
            @endif

            <div class="form-group">
                <label for="inputImagem">Imagem</small></label>
                <input type="file" class="form-control" id="inputImagem" name="imagem" required>
            </div>

            <input type="hidden" name="marcenaria_categorias_id" value="{{$marcenaria_categorias_id}}">

            <button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

            <a href="{{URL::route('painel.marcenariaImagens.index', array('marcenaria_categorias_id' => $marcenaria_categorias_id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

        </div>
    </form>

@stop