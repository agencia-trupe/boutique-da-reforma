@section('content')

    <legend>
        <h2><small>Home /</small> Editar Slide</h2>
    </legend>

    {{ Form::open( array('route' => array('painel.slides.update', $slide->id), 'files' => true, 'method' => 'put') ) }}
        <div class="pad">

            @if(Session::has('sucesso'))
               <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
            @endif

            @if($errors->any())
                <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
            @endif

            <div class="form-group">
                <label for="inputTitulo">Título</label>
                <input type="text" class="form-control" id="inputTitulo" name="titulo" value="{{$slide->titulo}}" required>
            </div>

            <div class="form-group">
                <label for="inputDesc">Descrição</label>
                <textarea name="descricao" id="inputDesc" class="form-control" rows="6" required>{{$slide->descricao}}</textarea>
            </div>

            <div class="form-group">
                <label for="inputLink">Link</label>
                <input type="text" class="form-control" id="inputLink" name="link" value="{{$slide->link}}" required>
            </div>

            <div class="form-group">
                <label>Imagem atual</label><br>
                <img src="{{url('assets/img/slides/'.$slide->imagem)}}" alt=""><br>
                <label for="inputImagem">Trocar Imagem <small>(214x214 px)</small></label>
                <input type="file" class="form-control" id="inputImagem" name="imagem">
            </div>

            <button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

            <a href="{{URL::route('painel.slides.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

        </div>
    </form>

@stop