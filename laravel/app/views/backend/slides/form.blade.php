@section('content')

    <legend>
        <h2><small>Home /</small> Adicionar Slide</h2>
    </legend>

    <form action="{{URL::route('painel.slides.store')}}" method="post" enctype="multipart/form-data">
        <div class="pad">

            @if(Session::has('sucesso'))
               <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
            @endif

            @if($errors->any())
                <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
            @endif

            <div class="form-group">
                <label for="inputTitulo">Título</label>
                <input type="text" class="form-control" id="inputTitulo" name="titulo"  @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
            </div>

            <div class="form-group">
                <label for="inputDesc">Descrição</label>
                <textarea name="descricao" id="inputDesc" class="form-control" rows="6" required>@if(Session::has('formulario')){{ Session::get('formulario.descricao') }}@endif</textarea>
            </div>

            <div class="form-group">
                <label for="inputLink">Link</label>
                <input type="text" class="form-control" id="inputLink" name="link" @if(Session::has('formulario')) value="{{ Session::get('formulario.link') }}" @endif required>
            </div>

            <div class="form-group">
                <label for="inputImagem">Imagem <small>(425x356 px)</small></label>
                <input type="file" class="form-control" id="inputImagem" name="imagem" required>
            </div>

            <button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

            <a href="{{URL::route('painel.slides.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

        </div>
    </form>

@stop