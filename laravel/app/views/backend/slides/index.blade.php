@section('content')

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <legend>
        <h2>
            <small>Home /</small> Slides
            <a href="{{ URL::route('painel.slides.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Slide</a>
        </h2>
    </legend>

    <table class="table table-bordered table-hover table-sortable" data-tabela="slides">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Imagem</th>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>
        <tbody>
            @foreach($slides as $slide)
            <tr class="tr-row" id="row_{{$slide->id}}">
                <td class="move-actions"><a href="#" class="btn btn-info btn-move btn-sm">mover</a></td>
                <td><img src="{{url('assets/img/slides/'.$slide->imagem)}}" style="max-width: 150px" alt=""></td>
                <td>{{ $slide->titulo }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.slides.edit', $slide->id ) }}' class='btn btn-primary btn-sm pull-left'>editar</a>
                    {{ Form::open(array('route' => array('painel.slides.destroy', $slide->id), 'method' => 'delete')) }}
                         <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                    {{ Form::close() }}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@stop