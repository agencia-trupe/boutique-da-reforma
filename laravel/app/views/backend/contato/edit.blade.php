@section('content')

    <legend>
        <h2>Editar Informações de Contato</h2>
    </legend>

    {{ Form::open( array('route' => array('painel.contato.update', $contato->id), 'files' => true, 'method' => 'put') ) }}
            <div class="pad">

            <div class="form-group">
                <label for="inputEmaildeContato">Email de Contato</label>
                <input type="text" class="form-control" id="inputEmaildeContato" name="email_contato" value="{{$contato->email_contato}}" required>
            </div>

            <div class="form-group">
                <label for="inputEmaildeContato">Telefone de Contato</label>
                <input type="text" class="form-control" id="inputTelefonedeContato" name="telefone_contato" value="{{$contato->telefone_contato}}" required>
            </div>

            <div class="form-group">
                <label for="inputFacebook">Facebook</label>
                <input type="text" class="form-control" id="inputFacebook" name="facebook" value="{{$contato->facebook}}" required>
            </div>

            <div class="form-group">
                <label for="inputInstagram">Twitter</label>
                <input type="text" class="form-control" id="inputTwitter" name="twitter" value="{{$contato->twitter}}">
            </div>

            <button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

            <a href="{{URL::route('painel.contato.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

        </div>
    </form>

@stop