@section('content')

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <legend>
        <h2>Informações de Contato</h2>
    </legend>

    <table class='table table-bordered table-hover'>
        <thead>
            <tr>
                <th>E-mail</th>
                <th>Telefone</th>
                <th>Facebook</th>
                <th>Twitter</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>
        <tbody>
            <tr class="tr-row">
                <td>{{ $contato->email_contato }}</td>
                <td>{{ $contato->telefone_contato }}</td>
                <td>
                    @if($contato->facebook)
                        <a href="{{$contato->facebook}}" title="Facebook" class="btn btn-sm btn-info" target="_blank">facebook</a>
                    @else
                        <button title="Não cadastrado" class="btn btn-sm btn-default" disabled="disabled">facebook</button>
                    @endif
                </td>
                <td>
                    @if($contato->twitter)
                        <a href="{{$contato->twitter}}" class="btn btn-sm btn-info" target="_blank">Twitter</a>
                    @else
                        <button title="Não cadastrado" class="btn btn-sm btn-default" disabled="disabled">twitter</button>
                    @endif
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.contato.edit', $contato->id ) }}' class='btn btn-primary btn-sm'>editar</a>
                </td>
            </tr>
        </tbody>
    </table>

@stop