@section('content')

    <legend>
        <h2><small>{{$categoria->titulo}} /</small> Adicionar Subcategoria</h2>
    </legend>

    <form action="{{URL::route('painel.reformaCategorias.store')}}" method="post" enctype="multipart/form-data">
        <div class="pad">

            @if(Session::has('sucesso'))
               <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
            @endif

            @if($errors->any())
                <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
            @endif

            <div class="form-group">
                <label for="inputTitulo">Título</label>
                <input type="text" class="form-control" id="inputTitulo" name="titulo"  @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
            </div>

            <div class="form-group">
                <label for="inputDesc">Descrição</label>
                <textarea name="descricao" id="inputDesc" class="form-control" rows="6" required>@if(Session::has('formulario')){{ Session::get('formulario.descricao') }}@endif</textarea>
            </div>

            <div class="form-group">
                <label for="inputImagem">Imagem de capa <small>(420x120 px)</small></label>
                <input type="file" class="form-control" id="inputImagem" name="imagem_capa" required>
            </div>

            <input type="hidden" name="reforma_id" value="{{$reforma_id}}">

            <button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

            <a href="{{URL::route('painel.reformaCategorias.index', array('reforma_id' => $reforma_id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

        </div>
    </form>

@stop