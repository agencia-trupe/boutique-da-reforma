@section('content')

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <a href="{{URL::route('painel.reforma.index')}}" title="Voltar para Reforma e Construção" class="btn btn-default">&larr; Voltar para Reforma e Construção</a>

    <legend>
        <h2>
            <small>Reforma e Construção / </small> {{$categoria->titulo}}
            <a href="{{ URL::route('painel.reformaCategorias.create', array('reforma_id' => $reforma_id)) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Subcategoria</a>
        </h2>
    </legend>

    @if(sizeof($subcategorias))
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Capa</th>
                    <th>Título</th>
                    <th>Imagens</th>
                    <th><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($subcategorias as $subcategoria)
                <tr>
                    <td><img src="{{url('assets/img/reforma/capa/'.$subcategoria->imagem_capa)}}" alt=""></td>
                    <td>{{$subcategoria->titulo}}</td>
                    <td>
                        <a href="{{URL::route('painel.reformaImagens.index', array('reforma_categoria_id' => $subcategoria->id))}}" class="btn btn-sm btn-default">gerenciar</a>
                    </td>
                    <td class="crud-actions">
                        <a href="{{ URL::route('painel.reformaCategorias.edit', array($subcategoria->id, 'reforma_id' => $reforma_id)) }}" class="btn btn-primary btn-sm pull-left">editar</a>
                       {{ Form::open(array('route' => array('painel.reformaCategorias.destroy', $subcategoria->id), 'method' => 'delete')) }}
                            <input type="hidden" name="reforma_id" value="{{$reforma_id}}">
                            <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="alert alert-warning" role="alert">Nenhuma subcategoria cadastrada, utilize o menu superior para inserir subcategorias.</div>
    @endif

@stop