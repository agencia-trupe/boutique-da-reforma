@section('content')

    <form action="{{ URL::route('painel.login') }}" method="post" class="form-signin">

        <h3 class="form-signin-heading">Boutique da Reforma<br><small>Painel Administrativo</small></h3>

        @if(Session::has('login_errors'))
            <div class="alert alert-danger">Usuário ou Senha inválidos.</div>
        @endif

        <input type="text" id="username" name="username" class="form-control" placeholder="Usuário" autofocus required>
        <input type="password" class="form-control" name="password" placeholder="Senha" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
    </form>

@stop