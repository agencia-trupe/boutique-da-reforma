@section('content')

    <legend>
        <h2>Adicionar Depoimento</h2>
    </legend>

    <form action="{{URL::route('painel.depoimentos.store')}}" method="post">
        <div class="pad">

            @if(Session::has('sucesso'))
               <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
            @endif

            @if($errors->any())
                <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
            @endif

            <div class="form-group">
                <label for="inputNome">Nome</label>
                <input type="text" class="form-control" id="inputNome" name="nome"  @if(Session::has('formulario')) value="{{ Session::get('formulario.nome') }}" @endif required>
            </div>

            <div class="form-group">
                <label for="inputDepoimento">Depoimento</label>
                <textarea name="texto" id="inputDepoimento" class="form-control" rows="6" required>@if(Session::has('formulario')){{ Session::get('formulario.texto') }}@endif</textarea>
            </div>

            <button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

            <a href="{{URL::route('painel.depoimentos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

        </div>
    </form>

@stop