@section('content')

    <legend>
        <h2>Editar Depoimento</h2>
    </legend>

    {{ Form::open( array('route' => array('painel.depoimentos.update', $depoimento->id), 'files' => true, 'method' => 'put') ) }}
        <div class="pad">

            @if(Session::has('sucesso'))
               <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
            @endif

            @if($errors->any())
                <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
            @endif

            <div class="form-group">
                <label for="inputNome">Nome</label>
                <input type="text" class="form-control" id="inputNome" name="nome" value="{{$depoimento->nome}}" required>
            </div>

            <div class="form-group">
                <label for="inputDepoimento">Depoimento</label>
                <textarea name="texto" id="inputDepoimento" class="form-control" rows="6" required>{{$depoimento->texto}}</textarea>
            </div>

            <button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

            <a href="{{URL::route('painel.depoimentos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

        </div>
    </form>

@stop