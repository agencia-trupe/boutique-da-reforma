@section('content')

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <legend>
        <h2>
            Depoimentos
            <a href="{{ URL::route('painel.depoimentos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Depoimento</a>
        </h2>
    </legend>

    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Texto</th>
                <th>Imagens</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($depoimentos as $depoimento)

            <tr class="tr-row">
                <td>
                    {{ $depoimento->nome }}
                </td>
                <td>
                    {{ $depoimento->texto }}
                </td>
                <td>
                    <a href="{{URL::route('painel.depoimentosImagens.index', array('depoimento_id' => $depoimento->id))}}" class="btn btn-sm btn-default">gerenciar</a>
                </td>
                <td class="crud-actions">
                    <a href="{{ URL::route('painel.depoimentos.edit', $depoimento->id ) }}" class="btn btn-primary btn-sm pull-left">editar</a>
                   {{ Form::open(array('route' => array('painel.depoimentos.destroy', $depoimento->id), 'method' => 'delete')) }}
                        <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                    </form>
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>
    {{$depoimentos->links()}}

@stop