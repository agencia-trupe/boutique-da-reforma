<?php

// Home

Route::get('/', array(
    'as'   => 'home',
    'uses' => 'HomeController@index'
));

Route::get('home', array(
    'as'   => 'home',
    'uses' => 'HomeController@index'
));


// Reforma & Construção

Route::get('reforma-construcao', array(
    'as'   => 'reforma',
    'uses' => 'ReformaController@index'
));

Route::get('reforma-construcao/{slug_categoria}', array(
    'as'   => 'reforma.categoria',
    'uses' => 'ReformaController@categoria'
));

Route::get('reforma-construcao/{slug_categoria}/{slug_subcategoria}', array(
    'as'   => 'reforma.subcategoria',
    'uses' => 'ReformaController@subcategoria'
));


// Decoração

Route::get('decoracao', array(
    'as'   => 'decoracao',
    'uses' => 'DecoracaoController@index'
));

Route::get('decoracao/{cat}', array(
    'as'   => 'decoracao.categoria',
    'uses' => 'DecoracaoController@categoria'
));

Route::get('decoracao/{cat}/{sub}', array(
    'as'   => 'decoracao.subcategoria',
    'uses' => 'DecoracaoController@subcategoria'
));


// Marcenaria

Route::get('marcenaria', array(
    'as'   => 'marcenaria',
    'uses' => 'MarcenariaController@index'
));

Route::get('marcenaria/{slug_categoria}', array(
    'as'   => 'marcenaria.categoria',
    'uses' => 'MarcenariaController@categoria'
));


// Fotos

Route::get('fotos', array(
    'as'   => 'fotos',
    'uses' => 'FotosController@index'
));


// Depoimentos

Route::get('depoimentos', array(
    'as'   => 'depoimentos',
    'uses' => 'DepoimentosController@index'
));


// Contato

Route::get('contato', array(
    'as'   => 'contato',
    'uses' => 'ContatoController@index'
));

Route::post('envioContato', array(
    'as'   => 'contato.envio',
    'uses' => 'ContatoController@envio'
));


// Painel --------------------------------------------------------------------

Route::get('painel', array(
    'before' => 'auth',
    'as'     => 'painel.home',
    'uses'   => 'Painel\HomeController@index'
));

Route::get('painel/login', array(
    'as'   => 'painel.login',
    'uses' => 'Painel\HomeController@login'
));

Route::post('painel/login',  array('as' => 'painel.auth', function() {
    $authvars = array(
        'username' => Input::get('username'),
        'password' => Input::get('password')
    );

    $lembrar = false;

    if (Auth::attempt($authvars, $lembrar)) {
        return Redirect::to('painel');
    } else {
        Session::flash('login_errors', true);
        return Redirect::to('painel/login');
    }
}));

Route::get('painel/logout', array('as' => 'painel.off', function() {
    Auth::logout();
    return Redirect::to('painel');
}));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function() {
    Route::resource('chamadas', 'Painel\ChamadasController');
    Route::resource('slides', 'Painel\SlidesController');

    Route::resource('reforma', 'Painel\ReformaController');
    Route::resource('reformaCategorias', 'Painel\ReformaCategoriasController');
    Route::resource('reformaImagens', 'Painel\ReformaImagensController');

    Route::resource('decoracao', 'Painel\DecoracaoController');
    Route::resource('decoracaoCategorias', 'Painel\DecoracaoCategoriasController');
    Route::resource('decoracaoImagens', 'Painel\DecoracaoImagensController');

    Route::resource('marcenaria', 'Painel\MarcenariaController');
    Route::resource('marcenariaCategorias', 'Painel\MarcenariaCategoriasController');
    Route::resource('marcenariaImagens', 'Painel\MarcenariaImagensController');

    Route::resource('fotos', 'Painel\FotosController');

    Route::resource('depoimentos', 'Painel\DepoimentosController');
    Route::resource('depoimentosImagens', 'Painel\DepoimentosImagensController');

    Route::resource('contato', 'Painel\ContatoController');
    Route::resource('contatosRecebidos', 'Painel\ContatosRecebidosController');

    Route::resource('usuarios', 'Painel\UsuariosController');

    Route::post('ajax/gravaOrdem', 'Painel\AjaxController@gravaOrdem');
});