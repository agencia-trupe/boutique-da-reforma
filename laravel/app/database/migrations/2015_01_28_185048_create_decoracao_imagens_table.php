<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDecoracaoImagensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('decoracao_imagens', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('imagem');
			$table->integer('decoracao_id');
			$table->integer('decoracao_categoria_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('decoracao_imagens');
	}

}
