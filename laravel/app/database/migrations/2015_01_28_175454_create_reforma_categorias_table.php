<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReformaCategoriasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reforma_categorias', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('slug');
			$table->string('titulo');
			$table->text('descricao');
			$table->string('imagem_capa');
			$table->integer('reforma_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reforma_categorias');
	}

}
