<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReformaImagensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reforma_imagens', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('imagem');
			$table->integer('reforma_id');
			$table->integer('reforma_categoria_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reforma_imagens');
	}

}
