<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        DB::table('contato')->delete();

        $data = array(
            array(
                'email_contato' => 'contato@boutiquedareforma.com.br',
                'telefone_contato' => '(11) 1234.5678',
                'facebook' => 'http://facebook.com/boutiquedareforma',
                'twitter' => 'http://twitter.com/bouticdareforma',
            )
        );

        DB::table('contato')->insert($data);
    }

}