<?php

use \DecoracaoCategoria, \DecoracaoImagem;

class DecoracaoController extends BaseController {

    protected $layout = 'frontend.template.index';

    public function index()
    {

        $decoracoes = Decoracao::get();

        $this->layout->content = View::make('frontend.decoracao.index')
            ->with(compact('decoracoes'));

    }

    public function categoria($slug_categoria)
    {

        $decoracoes  = Decoracao::get();

        $categoria = Decoracao::slug($slug_categoria)->first();
        if(!$categoria) App::abort('404');

        $subcategorias = DecoracaoCategoria::decoracao_id($categoria->id)->get();

        $imagens = Decoracao::find($categoria->id)->imagens;

        $this->layout->content = View::make('frontend.decoracao.categoria')
            ->with(compact('decoracoes'))
            ->with(compact('categoria'))
            ->with(compact('subcategorias'))
            ->with(compact('imagens'));

    }

    public function subcategoria($slug_categoria, $slug_subcategoria)
    {

        $decoracoes  = Decoracao::get();

        $categoria = Decoracao::slug($slug_categoria)->first();
        if(!$categoria) App::abort('404');

        $subcategoria = DecoracaoCategoria::slug($slug_subcategoria)->first();
        if(!$subcategoria) App::abort('404');

        $imagens = DecoracaoCategoria::find($subcategoria->id)->imagens()->orderBy('id', 'desc')->get();

        $this->layout->content = View::make('frontend.decoracao.subcategoria')
            ->with(compact('decoracoes'))
            ->with(compact('categoria'))
            ->with(compact('subcategoria'))
            ->with(compact('imagens'));

    }

}
