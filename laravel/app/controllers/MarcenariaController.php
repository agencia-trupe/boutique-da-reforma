<?php

use \Marcenaria, \MarcenariaCategoria, \MarcenariaImagem;

class MarcenariaController extends BaseController {

	protected $layout = 'frontend.template.index';

	public function index()
	{

        $marcenaria = Marcenaria::first();
        $categorias = MarcenariaCategoria::get();

        $imagens = MarcenariaImagem::where('marcenaria_categorias_id', '=', 0)->orderBy('id', 'desc')->get();

		$this->layout->content = View::make('frontend.marcenaria.index')
            ->with(compact('marcenaria'))
            ->with(compact('categorias'))
            ->with(compact('imagens'));

	}

    public function categoria($slug_categoria)
    {

        $marcenaria = Marcenaria::first();

        $categoria = MarcenariaCategoria::slug($slug_categoria)->first();
        if(!$categoria) App::abort('404');

        $imagens = MarcenariaCategoria::find($categoria->id)->imagens()->orderBy('id', 'desc')->get();

        $this->layout->content = View::make('frontend.marcenaria.categoria')
            ->with(compact('marcenaria'))
            ->with(compact('categoria'))
            ->with(compact('imagens'));
    }

}
