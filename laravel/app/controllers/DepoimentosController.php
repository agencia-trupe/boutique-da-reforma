<?php

class DepoimentosController extends BaseController {

	protected $layout = 'frontend.template.index';

	public function index()
	{

        $depoimentos = Depoimento::with('imagens')->orderBy('id', 'desc')->paginate(5);

		$this->layout->content = View::make('frontend.depoimentos.index')
            ->with(compact('depoimentos'));
	}

}
