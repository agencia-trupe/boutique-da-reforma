<?php

use \Contato, \Depoimento, \Reforma, \Decoracao;

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{

			View::share('reforma_categorias', Reforma::select('slug', 'titulo')->get());
			View::share('decoracao_categorias', Decoracao::select('slug', 'titulo')->get());

			View::share('depoimentos_footer', Depoimento::with('imagens')->orderByRaw("RAND()")->limit(3)->get());

			View::share('contato', Contato::first());

			$this->layout = View::make($this->layout);

		}
	}

}
