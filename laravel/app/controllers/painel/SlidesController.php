<?php

namespace Painel;

use \Slide, \View, \Input, \Session, \Redirect, \Image, \Validator;

class SlidesController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public function index()
    {
        $this->layout->content = View::make('backend.slides.index')
            ->with('slides', Slide::orderBy('ordem', 'asc')->get());
    }

    public function create() {
        $this->layout->content = View::make('backend.slides.form');
    }

    public function store() {
        $object = new Slide;

        $object->titulo = Input::get('titulo');
        $object->descricao = Input::get('descricao');
        $object->link = Input::get('link');

        $imagem = Input::file('imagem');

        $rules = array('imagem' => 'image');
        $validation = Validator::make(array('imagem' => $imagem), $rules);

        if ($validation->fails()) {

            Session::flash('formulario', Input::except('imagem'));
            return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));

        } else {

            $name = date('YmdHis').$imagem->getClientOriginalName();
            $path = 'assets/img/slides/'.$name;
            Image::make(Input::file('imagem')->getRealPath())->save($path, 100);
            $object->imagem = $name;

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Slide criado com sucesso.');
            return Redirect::route('painel.slides.index');

        } catch (\Exception $e) {

            Session::flash('formulario', Input::except('imagem'));
            return Redirect::back()->withErrors(array('Erro ao criar slide!'));

        }
    }

    public function show() {}

    public function edit($id)
    {
        $this->layout->content = View::make('backend.slides.edit')
            ->with('slide', Slide::find($id));
    }

    public function update($id)
    {
        $object = Slide::find($id);

        $object->titulo = Input::get('titulo');
        $object->link = Input::get('link');
        $object->descricao = Input::get('descricao');

        if (Input::hasFile('imagem')) {

            $imagem = Input::file('imagem');

            $rules = array('imagem' => 'image');
            $validation = Validator::make(array('imagem' => $imagem), $rules);

            if ($validation->fails()) {
                return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));
            } else {
                $name = date('YmdHis').$imagem->getClientOriginalName();
                $path = 'assets/img/slides/'.$name;
                Image::make(Input::file('imagem')->getRealPath())->save($path, 100);
                $object->imagem = $name;
            }

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Slide alterado com sucesso.');
            return Redirect::route('painel.slides.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(array('Erro ao alterar Chamada!'));

        }
    }

    public function destroy($id) {
        $object = Slide::find($id);
        $object->delete();

        Session::flash('sucesso', true);
        Session::flash('mensagem', 'Slide removido com sucesso.');

        return Redirect::route('painel.slides.index');
    }

}