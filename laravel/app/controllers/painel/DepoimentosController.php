<?php

namespace Painel;

use \Depoimento, \View, \Input, \Session, \Redirect, \Hash;

class DepoimentosController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public function index()
    {
        $this->layout->content = View::make('backend.depoimentos.index')
            ->with('depoimentos', Depoimento::orderBy('id', 'desc')->paginate(10));
    }

    public function create() {
        $this->layout->content = View::make('backend.depoimentos.form');
    }

    public function store() {
        $object = new Depoimento;

        $object->nome = Input::get('nome');
        $object->texto = Input::get('texto');

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Depoimento criado com sucesso.');
            return Redirect::route('painel.depoimentos.index');

        } catch (\Exception $e) {

            Session::flash('formulario', Input::all());
            return Redirect::back()->withErrors(array('Erro ao criar depoimento!'));

        }
    }

    public function show() {}

    public function edit($id)
    {
        $this->layout->content = View::make('backend.depoimentos.edit')
            ->with('depoimento', Depoimento::find($id));
    }

    public function update($id)
    {
        $object = Depoimento::find($id);

        $object->nome = Input::get('nome');
        $object->texto = Input::get('texto');

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Depoimento alterado com sucesso.');
            return Redirect::route('painel.depoimentos.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(array('Erro ao alterar Depoimento!'));

        }
    }

    public function destroy($id) {
        $object = Depoimento::find($id);
        $object->delete();

        Session::flash('sucesso', true);
        Session::flash('mensagem', 'Depoimento removido com sucesso.');

        return Redirect::route('painel.depoimentos.index');
    }

}