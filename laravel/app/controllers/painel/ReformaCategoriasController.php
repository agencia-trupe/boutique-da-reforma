<?php

namespace Painel;

use \Reforma, \ReformaCategoria, \View, \Input, \Session, \Redirect, \Image, \Validator, \Str;

class ReformaCategoriasController extends BaseAdminController {

    public function index()
    {
        $reforma_id = Input::get('reforma_id');

        $this->layout->content = View::make('backend.reformacategorias.index')
            ->with('subcategorias', ReformaCategoria::where('reforma_id', '=', $reforma_id)->get())
            ->with('categoria', Reforma::find($reforma_id))
            ->with('reforma_id', $reforma_id);
    }

    public function create()
    {
        $reforma_id = Input::get('reforma_id');

        if(!$reforma_id)
            return Redirect::back();

        $this->layout->content = View::make('backend.reformacategorias.form')
            ->with('reforma_id', $reforma_id)
            ->with('categoria', Reforma::find($reforma_id));
    }

    public function store() {
        $reforma_id = Input::get('reforma_id');

        $object = new ReformaCategoria;

        $object->titulo = Input::get('titulo');
        $object->descricao = Input::get('descricao');
        $object->slug = Str::slug(Input::get('titulo'));
        $object->reforma_id = $reforma_id;

        $imagem = Input::file('imagem_capa');

        $rules = array('imagem_capa' => 'image');
        $validation = Validator::make(array('imagem_capa' => $imagem), $rules);

        if (ReformaCategoria::where('reforma_id', '=', $reforma_id)->where('slug', '=', $object->slug)->count() > 0) {

            Session::flash('formulario', Input::except('imagem_capa'));
            return Redirect::back()->withErrors(array('Erro: Já existe outra subcategoria com o mesmo título'));

        }

        if ($validation->fails()) {

            Session::flash('formulario', Input::except('imagem_capa'));
            return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));

        } else {

            $name = date('YmdHis').$imagem->getClientOriginalName();
            $path = 'assets/img/reforma/capa/'.$name;

            $imgobj = Image::make(Input::file('imagem_capa')->getRealPath());
            $imgobj->fit(420, 120, function ($constraint) {
                $constraint->upsize();
            })->save($path, 100);

            $object->imagem_capa = $name;

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Subcategoria criada com sucesso.');
            return Redirect::route('painel.reformacategorias.index', array('reforma_id' => $reforma_id));

        } catch (\Exception $e) {

            Session::flash('formulario', Input::except('imagem_capa'));
            return Redirect::back()->withErrors(array('Erro ao criar subcategoria! Verifique se já existe outra subcategoria com o mesmo título'));

        }
    }

    public function show() {}

    public function edit($id)
    {
        $reforma_id = Input::get('reforma_id');

        $this->layout->content = View::make('backend.reformacategorias.edit')
            ->with('subcategoria', ReformaCategoria::find($id))
            ->with('reforma_id', $reforma_id);
    }

    public function update($id)
    {
        $reforma_id = Input::get('reforma_id');

        $object = ReformaCategoria::find($id);

        $object->titulo = Input::get('titulo');
        $object->descricao = Input::get('descricao');
        $object->slug = Str::slug(Input::get('titulo'));

        if (ReformaCategoria::where('reforma_id', '=', $reforma_id)->where('slug', '=', $object->slug)->count() > 0) {

            Session::flash('formulario', Input::except('imagem_capa'));
            return Redirect::back()->withErrors(array('Erro: Já existe outra subcategoria com o mesmo título'));

        }

        if (Input::hasFile('imagem_capa')) {

            $imagem = Input::file('imagem_capa');

            $rules = array('imagem_capa' => 'image');
            $validation = Validator::make(array('imagem_capa' => $imagem), $rules);

            if ($validation->fails()) {

                Session::flash('formulario', Input::except('imagem_capa'));
                return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));

            } else {

                $name = date('YmdHis').$imagem->getClientOriginalName();
                $path = 'assets/img/reforma/capa/'.$name;

                $imgobj = Image::make(Input::file('imagem_capa')->getRealPath());
                $imgobj->fit(420, 120, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);

                $object->imagem_capa = $name;

            }

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Subcategoria editada com sucesso.');
            return Redirect::route('painel.reformacategorias.index', array('reforma_id' => $reforma_id));

        } catch (\Exception $e) {

            Session::flash('formulario', Input::except('imagem_capa'));
            return Redirect::back()->withErrors(array('Erro ao editar subcategoria! Verifique se já existe outra subcategoria com o mesmo título'));

        }
    }

    public function destroy($id)
    {
        $reforma_id = Input::get('reforma_id');

        $object = ReformaCategoria::find($id);
        $object->delete();

        Session::flash('sucesso', true);
        Session::flash('mensagem', 'Subcategoria removida com sucesso.');

        return Redirect::route('painel.reformacategorias.index', array('reforma_id' => $reforma_id));
    }

}