<?php

namespace Painel;

use \Marcenaria, \MarcenariaCategoria, \View, \Input, \Session, \Redirect, \Image, \Validator;

class MarcenariaController extends BaseAdminController {

    public function index()
    {
        $this->layout->content = View::make('backend.marcenaria.index')
            ->with('marcenaria', Marcenaria::first())
            ->with('categorias', MarcenariaCategoria::get());
    }

    public function create() {}
    public function store() {}
    public function show($id) {}

    public function edit($id)
    {
        $this->layout->content = View::make('backend.marcenaria.edit')
            ->with('marcenaria', Marcenaria::find($id));
    }

    public function update($id)
    {
        $object = Marcenaria::find($id);

        $object->descricao = Input::get('descricao');

        if (Input::hasFile('imagem')) {

            $imagem = Input::file('imagem');

            $rules = array('imagem' => 'image');
            $validation = Validator::make(array('imagem' => $imagem), $rules);

            if ($validation->fails()) {
                return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));
            } else {
                $name = date('YmdHis').$imagem->getClientOriginalName();
                $path = 'assets/img/marcenaria/capa_home/'.$name;

                $imgobj = Image::make(Input::file('imagem')->getRealPath());
                $imgobj->fit(280, 250, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);

                $object->imagem_capa = $name;
            }

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Seção Marcenaria alterada com sucesso.');
            return Redirect::route('painel.marcenaria.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(array('Erro ao alterar seção!'));

        }
    }

    public function destroy($id) {}

}
