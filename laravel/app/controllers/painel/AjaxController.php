<?php

namespace Painel;

use \Request, \Input, \DB, \Controller;

class AjaxController extends Controller {

	function gravaOrdem()
	{

		if (!Request::ajax()) return false;

		$menu = Input::get('data');
		$tabela = Input::get('tabela');

        for ($i = 0; $i < count($menu); $i++) {
        	DB::table($tabela)->where('id', $menu[$i])->update(array('ordem' => $i));
        }

        return json_encode($menu);

	}
}