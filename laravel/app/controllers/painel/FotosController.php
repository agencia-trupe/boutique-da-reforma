<?php

namespace Painel;

use \Foto, \View, \Input, \Session, \Redirect, \Image, \Validator;

class FotosController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public function index()
    {
        $this->layout->content = View::make('backend.fotos.index')
            ->with('fotos', Foto::orderBy('id', 'desc')->paginate(10));
    }

    public function create() {
        $this->layout->content = View::make('backend.fotos.form');
    }

    public function store() {
        $object = new Foto;

        $imagem = Input::file('imagem');

        $rules = array('imagem' => 'image');
        $validation = Validator::make(array('imagem' => $imagem), $rules);

        if ($validation->fails()) {
            return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));
        } else {

            $name = date('YmdHis').$imagem->getClientOriginalName();
            $path = 'assets/img/fotos/'.$name;
            $path_t = 'assets/img/fotos/thumb/'.$name;

            $imgobj = Image::make(Input::file('imagem')->getRealPath());
            $ratio = $imgobj->width() / $imgobj->height();

            // ajusta largura ou altura dependendo da proporção
            if ($ratio > 1) {
                $imgobj->widen(800, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);
            } else {
                $imgobj->heighten(720, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);
            }

            // gera thumb 120x116
            $imgobj->fit(120, 116, function ($constraint) {
                $constraint->upsize();
            })->save($path_t, 100);

            $object->imagem = $name;

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Foto adicionada com sucesso.');
            return Redirect::route('painel.fotos.index');

        } catch (\Exception $e) {

            Session::flash('formulario', Input::all());
            return Redirect::back()->withErrors(array('Erro ao adicionar foto!'));

        }
    }

    public function show() {}
    public function edit($id) {}
    public function update($id) {}

    public function destroy($id) {
        $object = Foto::find($id);
        $object->delete();

        Session::flash('sucesso', true);
        Session::flash('mensagem', 'Foto removida com sucesso.');

        return Redirect::route('painel.fotos.index');
    }

}