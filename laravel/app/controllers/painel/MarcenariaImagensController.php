<?php

namespace Painel;

use \MarcenariaCategoria, \MarcenariaImagem, \View, \Input, \Session, \Redirect, \Validator, \Image;

class MarcenariaImagensController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public function index()
    {
        $marcenaria_categorias_id = (Input::get('marcenaria_categorias_id') ?: 0);

        $categoria = ($marcenaria_categorias_id == 0 ? 'Marcenaria' : MarcenariaCategoria::find($marcenaria_categorias_id));

        $this->layout->content = View::make('backend.marcenariaimagens.index')
            ->with('imagens', MarcenariaImagem::orderBy('id', 'desc')->where('marcenaria_categorias_id', $marcenaria_categorias_id)->get())
            ->with(compact('categoria'))
            ->with(compact('marcenaria_categorias_id'));
    }

    public function create() {
        $marcenaria_categorias_id = (Input::get('marcenaria_categorias_id') ?: 0);

        $categoria = ($marcenaria_categorias_id == 0 ? 'Marcenaria' : MarcenariaCategoria::find($marcenaria_categorias_id));

        $this->layout->content = View::make('backend.marcenariaimagens.form')
            ->with(compact('categoria'))
            ->with(compact('marcenaria_categorias_id'));
    }

    public function store() {
        $marcenaria_categorias_id = Input::get('marcenaria_categorias_id');

        $object = new MarcenariaImagem;

        $imagem = Input::file('imagem');
        $object->marcenaria_categorias_id = $marcenaria_categorias_id;

        $rules = array('imagem' => 'image');
        $validation = Validator::make(array('imagem' => $imagem), $rules);

        if ($validation->fails()) {
            return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));
        } else {

            $name = date('YmdHis').$imagem->getClientOriginalName();
            $path = 'assets/img/marcenaria/'.$name;
            $path_t = 'assets/img/marcenaria/thumb/'.$name;

            $imgobj = Image::make(Input::file('imagem')->getRealPath());
            $ratio = $imgobj->width() / $imgobj->height();

            // ajusta largura ou altura dependendo da proporção
            if ($ratio > 1) {
                $imgobj->widen(800, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);
            } else {
                $imgobj->heighten(720, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);
            }

            // gera thumb 120x116
            $imgobj->fit(120, 116, function ($constraint) {
                $constraint->upsize();
            })->save($path_t, 100);

            $object->imagem = $name;

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Imagem adicionada com sucesso.');
            return Redirect::route('painel.marcenariaimagens.index', array('marcenaria_categorias_id' => $marcenaria_categorias_id));

        } catch (\Exception $e) {

            Session::flash('formulario', Input::all());
            return Redirect::back()->withErrors(array('Erro ao adicionar imagem!'));

        }
    }
    public function show() {}
    public function edit($id) {}
    public function update($id) {}

    public function destroy($id) {
        $marcenaria_categorias_id = Input::get('marcenaria_categorias_id');

        $object = MarcenariaImagem::find($id);
        $object->delete();

        Session::flash('sucesso', true);
        Session::flash('mensagem', 'Imagem removida com sucesso.');

        return Redirect::route('painel.marcenariaimagens.index', array('marcenaria_categorias_id' => $marcenaria_categorias_id));
    }

}