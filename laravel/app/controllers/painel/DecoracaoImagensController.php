<?php

namespace Painel;

use \Decoracao, \DecoracaoCategoria, \DecoracaoImagem, \View, \Input, \Session, \Redirect, \Validator, \Image;

class DecoracaoImagensController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public function index()
    {
        $decoracao_id = Input::get('decoracao_id');
        $decoracao_categoria_id = Input::get('decoracao_categoria_id');

        $current = ($decoracao_id
            ? array(
                'titulo' => Decoracao::find($decoracao_id)->titulo,
                'id' => $decoracao_id,
                'property' => 'decoracao_id'
                )
            : array (
                'titulo' => DecoracaoCategoria::find($decoracao_categoria_id)->titulo,
                'id' => $decoracao_categoria_id,
                'property' => 'decoracao_categoria_id'
                )
            );

        $this->layout->content = View::make('backend.decoracaoimagens.index')
            ->with('imagens', DecoracaoImagem::orderBy('id', 'desc')->where($current['property'], $current['id'])->get())
            ->with(compact('current'));
    }

    public function create() {
        $decoracao_id = Input::get('decoracao_id');
        $decoracao_categoria_id = Input::get('decoracao_categoria_id');

        $current = ($decoracao_id
            ? array(
                'titulo' => Decoracao::find($decoracao_id)->titulo,
                'id' => $decoracao_id,
                'property' => 'decoracao_id'
                )
            : array (
                'titulo' => DecoracaoCategoria::find($decoracao_categoria_id)->titulo,
                'id' => $decoracao_categoria_id,
                'property' => 'decoracao_categoria_id'
                )
            );

        $this->layout->content = View::make('backend.decoracaoimagens.form')
            ->with(compact('current'));
    }

    public function store() {
        $decoracao_id = (Input::get('decoracao_id') ? Input::get('decoracao_id') : 0);
        $decoracao_categoria_id = (Input::get('decoracao_categoria_id') ? Input::get('decoracao_categoria_id') : 0);

        $current = ($decoracao_id
            ? array(
                'titulo' => Decoracao::find($decoracao_id)->titulo,
                'id' => $decoracao_id,
                'property' => 'decoracao_id'
                )
            : array (
                'titulo' => DecoracaoCategoria::find($decoracao_categoria_id)->titulo,
                'id' => $decoracao_categoria_id,
                'property' => 'decoracao_categoria_id'
                )
            );

        $object = new DecoracaoImagem;

        $imagem = Input::file('imagem');
        $object->decoracao_id = $decoracao_id;
        $object->decoracao_categoria_id = $decoracao_categoria_id;

        $rules = array('imagem' => 'image');
        $validation = Validator::make(array('imagem' => $imagem), $rules);

        if ($validation->fails()) {
            return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));
        } else {

            $name = date('YmdHis').$imagem->getClientOriginalName();
            $path = 'assets/img/decoracao/'.$name;
            $path_t = 'assets/img/decoracao/thumb/'.$name;

            $imgobj = Image::make(Input::file('imagem')->getRealPath());
            $ratio = $imgobj->width() / $imgobj->height();

            // ajusta largura ou altura dependendo da proporção
            if ($ratio > 1) {
                $imgobj->widen(800, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);
            } else {
                $imgobj->heighten(720, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);
            }

            // gera thumb 120x116
            $imgobj->fit(120, 116, function ($constraint) {
                $constraint->upsize();
            })->save($path_t, 100);

            $object->imagem = $name;

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Imagem adicionada com sucesso.');
            return Redirect::route('painel.decoracaoimagens.index', array($current['property'] => $current['id']));

        } catch (\Exception $e) {

            Session::flash('formulario', Input::all());
            return Redirect::back()->withErrors(array('Erro ao adicionar imagem!'));

        }
    }
    public function show() {}
    public function edit($id) {}
    public function update($id) {}

    public function destroy($id) {
        $route_property = Input::get('route_property');
        $route_id = Input::get('route_id');

        $object = DecoracaoImagem::find($id);
        $object->delete();

        Session::flash('sucesso', true);
        Session::flash('mensagem', 'Imagem removida com sucesso.');

        return Redirect::route('painel.decoracaoimagens.index', array($route_property => $route_id));
    }

}