<?php

namespace Painel;

use \Reforma, \ReformaCategoria, \ReformaImagem, \View, \Input, \Session, \Redirect, \Validator, \Image;

class ReformaImagensController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public function index()
    {
        $reforma_id = Input::get('reforma_id');
        $reforma_categoria_id = Input::get('reforma_categoria_id');

        $current = ($reforma_id
            ? array(
                'titulo' => Reforma::find($reforma_id)->titulo,
                'id' => $reforma_id,
                'property' => 'reforma_id'
                )
            : array (
                'titulo' => ReformaCategoria::find($reforma_categoria_id)->titulo,
                'id' => $reforma_categoria_id,
                'property' => 'reforma_categoria_id'
                )
            );

        $this->layout->content = View::make('backend.reformaimagens.index')
            ->with('imagens', ReformaImagem::orderBy('id', 'desc')->where($current['property'], $current['id'])->get())
            ->with(compact('current'));
    }

    public function create() {
        $reforma_id = Input::get('reforma_id');
        $reforma_categoria_id = Input::get('reforma_categoria_id');

        $current = ($reforma_id
            ? array(
                'titulo' => Reforma::find($reforma_id)->titulo,
                'id' => $reforma_id,
                'property' => 'reforma_id'
                )
            : array (
                'titulo' => ReformaCategoria::find($reforma_categoria_id)->titulo,
                'id' => $reforma_categoria_id,
                'property' => 'reforma_categoria_id'
                )
            );

        $this->layout->content = View::make('backend.reformaimagens.form')
            ->with(compact('current'));
    }

    public function store() {
        $reforma_id = (Input::get('reforma_id') ? Input::get('reforma_id') : 0);
        $reforma_categoria_id = (Input::get('reforma_categoria_id') ? Input::get('reforma_categoria_id') : 0);

        $current = ($reforma_id
            ? array(
                'titulo' => Reforma::find($reforma_id)->titulo,
                'id' => $reforma_id,
                'property' => 'reforma_id'
                )
            : array (
                'titulo' => ReformaCategoria::find($reforma_categoria_id)->titulo,
                'id' => $reforma_categoria_id,
                'property' => 'reforma_categoria_id'
                )
            );

        $object = new ReformaImagem;

        $imagem = Input::file('imagem');
        $object->reforma_id = $reforma_id;
        $object->reforma_categoria_id = $reforma_categoria_id;

        $rules = array('imagem' => 'image');
        $validation = Validator::make(array('imagem' => $imagem), $rules);

        if ($validation->fails()) {
            return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));
        } else {

            $name = date('YmdHis').$imagem->getClientOriginalName();
            $path = 'assets/img/reforma/'.$name;
            $path_t = 'assets/img/reforma/thumb/'.$name;

            $imgobj = Image::make(Input::file('imagem')->getRealPath());
            $ratio = $imgobj->width() / $imgobj->height();

            // ajusta largura ou altura dependendo da proporção
            if ($ratio > 1) {
                $imgobj->widen(800, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);
            } else {
                $imgobj->heighten(720, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);
            }

            // gera thumb 120x116
            $imgobj->fit(120, 116, function ($constraint) {
                $constraint->upsize();
            })->save($path_t, 100);

            $object->imagem = $name;

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Imagem adicionada com sucesso.');
            return Redirect::route('painel.reformaimagens.index', array($current['property'] => $current['id']));

        } catch (\Exception $e) {

            Session::flash('formulario', Input::all());
            return Redirect::back()->withErrors(array('Erro ao adicionar imagem!'));

        }
    }
    public function show() {}
    public function edit($id) {}
    public function update($id) {}

    public function destroy($id) {
        $route_property = Input::get('route_property');
        $route_id = Input::get('route_id');

        $object = ReformaImagem::find($id);
        $object->delete();

        Session::flash('sucesso', true);
        Session::flash('mensagem', 'Imagem removida com sucesso.');

        return Redirect::route('painel.reformaimagens.index', array($route_property => $route_id));
    }

}