<?php

namespace Painel;

use \Decoracao, \DecoracaoCategoria, \View, \Input, \Session, \Redirect, \Image, \Validator, \Str;

class DecoracaoCategoriasController extends BaseAdminController {

    public function index()
    {
        $decoracao_id = Input::get('decoracao_id');

        $this->layout->content = View::make('backend.decoracaocategorias.index')
            ->with('subcategorias', DecoracaoCategoria::where('decoracao_id', '=', $decoracao_id)->get())
            ->with('categoria', Decoracao::find($decoracao_id))
            ->with('decoracao_id', $decoracao_id);
    }

    public function create()
    {
        $decoracao_id = Input::get('decoracao_id');

        if(!$decoracao_id)
            return Redirect::back();

        $this->layout->content = View::make('backend.decoracaocategorias.form')
            ->with('decoracao_id', $decoracao_id)
            ->with('categoria', Decoracao::find($decoracao_id));
    }

    public function store() {
        $decoracao_id = Input::get('decoracao_id');

        $object = new DecoracaoCategoria;

        $object->titulo = Input::get('titulo');
        $object->descricao = Input::get('descricao');
        $object->slug = Str::slug(Input::get('titulo'));
        $object->decoracao_id = $decoracao_id;

        $imagem = Input::file('imagem_capa');

        $rules = array('imagem_capa' => 'image');
        $validation = Validator::make(array('imagem_capa' => $imagem), $rules);

        if (DecoracaoCategoria::where('decoracao_id', '=', $decoracao_id)->where('slug', '=', $object->slug)->count() > 0) {

            Session::flash('formulario', Input::except('imagem_capa'));
            return Redirect::back()->withErrors(array('Erro: Já existe outra subcategoria com o mesmo título'));

        }

        if ($validation->fails()) {

            Session::flash('formulario', Input::except('imagem_capa'));
            return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));

        } else {

            $name = date('YmdHis').$imagem->getClientOriginalName();
            $path = 'assets/img/decoracao/capa/'.$name;

            $imgobj = Image::make(Input::file('imagem_capa')->getRealPath());
            $imgobj->fit(420, 120, function ($constraint) {
                $constraint->upsize();
            })->save($path, 100);

            $object->imagem_capa = $name;

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Subcategoria criada com sucesso.');
            return Redirect::route('painel.decoracaocategorias.index', array('decoracao_id' => $decoracao_id));

        } catch (\Exception $e) {

            Session::flash('formulario', Input::except('imagem_capa'));
            return Redirect::back()->withErrors(array('Erro ao criar subcategoria! Verifique se já existe outra subcategoria com o mesmo título'));

        }
    }

    public function show() {}

    public function edit($id)
    {
        $decoracao_id = Input::get('decoracao_id');

        $this->layout->content = View::make('backend.decoracaocategorias.edit')
            ->with('subcategoria', DecoracaoCategoria::find($id))
            ->with('decoracao_id', $decoracao_id);
    }

    public function update($id)
    {
        $decoracao_id = Input::get('decoracao_id');

        $object = DecoracaoCategoria::find($id);

        $object->titulo = Input::get('titulo');
        $object->descricao = Input::get('descricao');
        $object->slug = Str::slug(Input::get('titulo'));

        if (DecoracaoCategoria::where('decoracao_id', '=', $decoracao_id)->where('slug', '=', $object->slug)->count() > 0) {

            Session::flash('formulario', Input::except('imagem_capa'));
            return Redirect::back()->withErrors(array('Erro: Já existe outra subcategoria com o mesmo título'));

        }

        if (Input::hasFile('imagem_capa')) {

            $imagem = Input::file('imagem_capa');

            $rules = array('imagem_capa' => 'image');
            $validation = Validator::make(array('imagem_capa' => $imagem), $rules);

            if ($validation->fails()) {

                Session::flash('formulario', Input::except('imagem_capa'));
                return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));

            } else {

                $name = date('YmdHis').$imagem->getClientOriginalName();
                $path = 'assets/img/decoracao/capa/'.$name;

                $imgobj = Image::make(Input::file('imagem_capa')->getRealPath());
                $imgobj->fit(420, 120, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);

                $object->imagem_capa = $name;

            }

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Subcategoria editada com sucesso.');
            return Redirect::route('painel.decoracaocategorias.index', array('decoracao_id' => $decoracao_id));

        } catch (\Exception $e) {

            Session::flash('formulario', Input::except('imagem_capa'));
            return Redirect::back()->withErrors(array('Erro ao editar subcategoria! Verifique se já existe outra subcategoria com o mesmo título'));

        }
    }

    public function destroy($id)
    {
        $decoracao_id = Input::get('decoracao_id');

        $object = DecoracaoCategoria::find($id);
        $object->delete();

        Session::flash('sucesso', true);
        Session::flash('mensagem', 'Subcategoria removida com sucesso.');

        return Redirect::route('painel.decoracaocategorias.index', array('decoracao_id' => $decoracao_id));
    }

}