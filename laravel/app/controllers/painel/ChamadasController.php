<?php

namespace Painel;

use \Chamada, \View, \Input, \Session, \Redirect, \Image, \Validator;

class ChamadasController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public function index()
    {
        $this->layout->content = View::make('backend.chamadas.index')
            ->with('chamadas', Chamada::all());
    }

    public function create() {}
    public function store() {}
    public function show() {}

    public function edit($id)
    {
        $this->layout->content = View::make('backend.chamadas.edit')
            ->with('chamada', Chamada::find($id));
    }

    public function update($id)
    {
        $object = Chamada::find($id);

        $object->titulo = Input::get('titulo');
        $object->link = Input::get('link');
        $object->descricao = Input::get('descricao');

        if (Input::hasFile('imagem')) {

            $imagem = Input::file('imagem');

            $rules = array('imagem' => 'image');
            $validation = Validator::make(array('imagem' => $imagem), $rules);

            if ($validation->fails()) {
                return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));
            } else {
                $name = date('YmdHis').$imagem->getClientOriginalName();
                $path = 'assets/img/chamadas/'.$name;
                Image::make(Input::file('imagem')->getRealPath())->save($path, 100);
                $object->imagem = $name;
            }

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Chamada alterada com sucesso.');
            return Redirect::route('painel.chamadas.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(array('Erro ao alterar Chamada!'));

        }
    }

    public function destroy() {}

}