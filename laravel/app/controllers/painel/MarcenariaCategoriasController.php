<?php

namespace Painel;

use \MarcenariaCategoria, \MarcenariaImagem, \View, \Input, \Session, \Redirect, \Validator, \Image, \Str;

class MarcenariaCategoriasController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public function index()
    {
        return Redirect::route('painel.marcenaria.index');
    }

    public function create() {
        $this->layout->content = View::make('backend.marcenariacategorias.form')
            ->with('categorias', MarcenariaCategoria::get());
    }

    public function store() {
        $object = new MarcenariaCategoria;

        $object->titulo = Input::get('titulo');
        $object->descricao = Input::get('descricao');
        $object->slug = Str::slug(Input::get('titulo'));

        $imagem = Input::file('imagem_capa');

        $rules = array('imagem_capa' => 'image');
        $validation = Validator::make(array('imagem_capa' => $imagem), $rules);

        if ($validation->fails()) {

            Session::flash('formulario', Input::except('imagem_capa'));
            return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));

        } else {

            $name = date('YmdHis').$imagem->getClientOriginalName();
            $path = 'assets/img/marcenaria/capa/'.$name;

            $imgobj = Image::make(Input::file('imagem_capa')->getRealPath());
            $imgobj->fit(420, 120, function ($constraint) {
                $constraint->upsize();
            })->save($path, 100);

            $object->imagem_capa = $name;

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Categoria criada com sucesso.');
            return Redirect::route('painel.marcenaria.index');

        } catch (\Exception $e) {

            Session::flash('formulario', Input::except('imagem_capa'));
            return Redirect::back()->withErrors(array('Erro ao criar categoria! Verifique se já existe outra categoria com o mesmo título'));

        }
    }

    public function show() {}

    public function edit($id) {
        $this->layout->content = View::make('backend.marcenariacategorias.edit')
            ->with('categoria', MarcenariaCategoria::find($id));
    }

    public function update($id) {
        $object = MarcenariaCategoria::find($id);

        $object->titulo = Input::get('titulo');
        $object->descricao = Input::get('descricao');
        $object->slug = Str::slug(Input::get('titulo'));

        if (Input::hasFile('imagem')) {

            $imagem = Input::file('imagem_capa');

            $rules = array('imagem_capa' => 'image');
            $validation = Validator::make(array('imagem_capa' => $imagem), $rules);

            if ($validation->fails()) {

                Session::flash('formulario', Input::except('imagem_capa'));
                return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));

            } else {

                $name = date('YmdHis').$imagem->getClientOriginalName();
                $path = 'assets/img/marcenaria/capa/'.$name;

                $imgobj = Image::make(Input::file('imagem_capa')->getRealPath());
                $imgobj->fit(420, 120, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);

                $object->imagem_capa = $name;

            }

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Categoria alterada com sucesso.');
            return Redirect::route('painel.marcenaria.index');

        } catch (\Exception $e) {

            Session::flash('formulario', Input::except('imagem_capa'));
            return Redirect::back()->withErrors(array('Erro ao alterar categoria! Verifique se já existe outra categoria com o mesmo título'));

        }
    }

    public function destroy($id) {
        $object = MarcenariaCategoria::find($id);
        $object->delete();

        Session::flash('sucesso', true);
        Session::flash('mensagem', 'Categoria removida com sucesso.');

        return Redirect::route('painel.marcenaria.index');
    }

}