<?php

namespace Painel;

use \Reforma, \View, \Input, \Session, \Redirect, \Image, \Validator, \Str;

class ReformaController extends BaseAdminController {

    public function index()
    {
        $this->layout->content = View::make('backend.reforma.index')
            ->with('categorias', Reforma::get());
    }

    public function create()
    {
        $this->layout->content = View::make('backend.reforma.form');
    }

    public function store()
    {
        $object = new Reforma;

        $object->titulo = Input::get('titulo');
        $object->olho = Input::get('olho');
        $object->descricao = Input::get('descricao');
        $object->slug = Str::slug(Input::get('titulo'));

        $imagem = Input::file('thumb');

        $rules = array('thumb' => 'image');
        $validation = Validator::make(array('thumb' => $imagem), $rules);

        if ($validation->fails()) {

            Session::flash('formulario', Input::except('thumb'));
            return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));

        } else {

            $name = date('YmdHis').$imagem->getClientOriginalName();
            $path = 'assets/img/reforma/thumb_home/'.$name;
            Image::make(Input::file('thumb')->getRealPath())->save($path, 100);
            $object->thumb = $name;

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Categoria criada com sucesso.');
            return Redirect::route('painel.reforma.index');

        } catch (\Exception $e) {

            Session::flash('formulario', Input::except('thumb'));
            return Redirect::back()->withErrors(array('Erro ao criar categoria! Verifique se já existe outra categoria com o mesmo título'));

        }
    }

    public function show() {}

    public function edit($id)
    {
        $this->layout->content = View::make('backend.reforma.edit')
            ->with('categoria', Reforma::find($id));
    }

    public function update($id)
    {
        $object = Reforma::find($id);

        $object->titulo = Input::get('titulo');
        $object->olho = Input::get('olho');
        $object->descricao = Input::get('descricao');
        $object->slug = Str::slug(Input::get('titulo'));

        if (Input::hasFile('thumb')) {

            $imagem = Input::file('thumb');

            $rules = array('thumb' => 'image');
            $validation = Validator::make(array('thumb' => $imagem), $rules);

            if ($validation->fails()) {

                Session::flash('formulario', Input::except('thumb'));
                return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));

            } else {

                $name = date('YmdHis').$imagem->getClientOriginalName();
                $path = 'assets/img/reforma/thumb_home/'.$name;
                Image::make(Input::file('thumb')->getRealPath())->save($path, 100);
                $object->thumb = $name;

            }

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Categoria alterada com sucesso.');
            return Redirect::route('painel.reforma.index');

        } catch (\Exception $e) {

            Session::flash('formulario', Input::except('thumb'));
            return Redirect::back()->withErrors(array('Erro ao alterar categoria! Verifique se já existe outra categoria com o mesmo título'));

        }
    }

    public function destroy($id) {
        $object = Reforma::find($id);
        $object->delete();

        Session::flash('sucesso', true);
        Session::flash('mensagem', 'Categoria removida com sucesso.');

        return Redirect::route('painel.reforma.index');
    }

}