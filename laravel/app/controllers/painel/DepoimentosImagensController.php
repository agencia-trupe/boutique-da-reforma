<?php

namespace Painel;

use \Depoimento, \DepoimentoImagem, \View, \Input, \Session, \Redirect, \Validator, \Image;

class DepoimentosImagensController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public $limiteRegistros = 5;

    public function index()
    {
        $depoimento_id = Input::get('depoimento_id');

        if(!$depoimento_id)
            return Redirect::back();

        $this->layout->content = View::make('backend.depoimentosimagens.index')
            ->with('imagens', DepoimentoImagem::orderBy('id', 'desc')->where('depoimento_id', $depoimento_id)->get())
            ->with('depoimento', Depoimento::find($depoimento_id))
            ->with('limiteRegistros', $this->limiteRegistros);
    }

    public function create() {
        $depoimento_id = Input::get('depoimento_id');

        if(!$depoimento_id)
            return Redirect::back();

        $this->layout->content = View::make('backend.depoimentosimagens.form')
            ->with('depoimento', Depoimento::find($depoimento_id));
    }

    public function store() {
        $depoimento_id = Input::get('depoimento_id');

        $object = new DepoimentoImagem;

        $imagem = Input::file('imagem');
        $object->depoimento_id = $depoimento_id;

        if(sizeof(DepoimentoImagem::where('depoimento_id', '=', $object->depoimento_id)->get()) >= $this->limiteRegistros){
            return Redirect::back()->withErrors(array('Número máximo de imagens já atingido!'));
        }

        $rules = array('imagem' => 'image');
        $validation = Validator::make(array('imagem' => $imagem), $rules);

        if ($validation->fails()) {
            return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));
        } else {

            $name = date('YmdHis').$imagem->getClientOriginalName();
            $path = 'assets/img/depoimentos/'.$name;
            $path_t = 'assets/img/depoimentos/thumb/'.$name;

            $imgobj = Image::make(Input::file('imagem')->getRealPath());
            $ratio = $imgobj->width() / $imgobj->height();

            // ajusta largura ou altura dependendo da proporção
            if ($ratio > 1) {
                $imgobj->widen(800, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);
            } else {
                $imgobj->heighten(720, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);
            }

            // gera thumb 120x116
            $imgobj->fit(120, 116, function ($constraint) {
                $constraint->upsize();
            })->save($path_t, 100);

            $object->imagem = $name;

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Imagem adicionada com sucesso.');
            return Redirect::route('painel.depoimentosimagens.index', array('depoimento_id' => $depoimento_id));

        } catch (\Exception $e) {

            Session::flash('formulario', Input::all());
            return Redirect::back()->withErrors(array('Erro ao adicionar imagem!'));

        }
    }
    public function show() {}
    public function edit($id) {}
    public function update($id) {}

    public function destroy($id) {
        $depoimento_id = Input::get('depoimento_id');

        $object = DepoimentoImagem::find($id);
        $object->delete();

        Session::flash('sucesso', true);
        Session::flash('mensagem', 'Imagem removida com sucesso.');

        return Redirect::route('painel.depoimentosimagens.index', array('depoimento_id' => $depoimento_id));
    }

}