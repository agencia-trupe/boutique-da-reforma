<?php

use \ReformaCategoria, \ReformaImagem;

class ReformaController extends BaseController {

	protected $layout = 'frontend.template.index';

	public function index()
	{

        $reformas = Reforma::get();

		$this->layout->content = View::make('frontend.reforma.index')
            ->with(compact('reformas'));

	}

    public function categoria($slug_categoria)
    {

        $reformas  = Reforma::get();

        $categoria = Reforma::slug($slug_categoria)->first();
        if(!$categoria) App::abort('404');

        $subcategorias = ReformaCategoria::reforma_id($categoria->id)->get();

        $imagens = Reforma::find($categoria->id)->imagens;

        $this->layout->content = View::make('frontend.reforma.categoria')
            ->with(compact('reformas'))
            ->with(compact('categoria'))
            ->with(compact('subcategorias'))
            ->with(compact('imagens'));

    }

    public function subcategoria($slug_categoria, $slug_subcategoria)
    {

        $reformas  = Reforma::get();

        $categoria = Reforma::slug($slug_categoria)->first();
        if(!$categoria) App::abort('404');

        $subcategoria = ReformaCategoria::slug($slug_subcategoria)->first();
        if(!$subcategoria) App::abort('404');

        $imagens = ReformaCategoria::find($subcategoria->id)->imagens()->orderBy('id', 'desc')->get();

        $this->layout->content = View::make('frontend.reforma.subcategoria')
            ->with(compact('reformas'))
            ->with(compact('categoria'))
            ->with(compact('subcategoria'))
            ->with(compact('imagens'));

    }

}
