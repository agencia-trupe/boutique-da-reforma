<?php

use \Chamada, \Slide;

class HomeController extends BaseController {

	protected $layout = 'frontend.template.index';

	public function index()
	{

        $chamadas = Chamada::take(3)->get();
        $slides   = Slide::orderBy('ordem', 'asc')->get();

		$this->layout->content = View::make('frontend.home.index')
            ->with(compact('chamadas'))
            ->with(compact('slides'));

	}

}
