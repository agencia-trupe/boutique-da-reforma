<?php

use \Foto;

class FotosController extends BaseController {

	protected $layout = 'frontend.template.index';

	public function index()
	{

        $fotos = Foto::orderBy('id', 'desc')->get();

		$this->layout->content = View::make('frontend.fotos.index')
            ->with(compact('fotos'));
	}

}
